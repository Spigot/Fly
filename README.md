# Fly

A Minecraft plugin to allow the toggling of flight and fly speed

## Commands

### Flight
Toggle your ability to fly

|||
|-|-|
|Permission|fly.toggle.self|
|Alias'|fly, flight|
|usage|/fly <player(s)>|


#### Others
You can toggle the flight of others by appending the `/fly` command with the player names.

Permission: `fly.toggle.other`

For instance:

`/fly player1 player2`

Both player1 and player2 will have their flight toggled

### Flight Speed
Modify your flight speed

|||
|-|-|
|Permission|flyspeed.toggle.self|
|Alias'|flyspeed, fspeed, fs|
|usage|/flyspeed [amount] <player(s)>|


#### Others
Setting other players flight speed is super simple, just append the player's names to the end of the command just as you do for `/fly`

Permission: `flyspeed.toggle.other`

## Configuration

|Path|Default|Description|
|-|-|-|
|fly.self.enabled|§0[§a✓§0]§r You can now fly|Displayed when a player can now fly|
|fly.self.disabled|§0[§c!§0]§r You can no longer fly|Displayed when a player can no longer fly|
|fly.other.enabled|§0[§a✓§0]§r Their flight has now been enabled|Displayed when the requested player's flight is enabled|
|fly.other.disabled|§0[§a✓§0]§r Their flight is now disabled|Displayed when the requested player's flight is disabled|
|fly.other.nopermission|§0[§c!§0]§r You don't have permission to do that|Displayed when the player doesn't have `fly.toggle.other`|
|fly.other.notfound|§0[§c!§0]§r Unkown player|Displayed when a requested player doesn't exist|
|flyspeed.self.set|§0[§a✓§0]§r Your flight speed has been changed|Displayed when the players flight speed is changed|
|flyspeed.other.set|§0[§a✓§0]§r Their flight speed has been changed|Displayed when the requested player's flight speed is changed|
|flyspeed.other.nopermission|§0[§c!§0]§r You don't have permission to do that|Displayed when the player doesn't have `flyspeed.toggle.other`|
|flyspeed.other.notfound|§0[§c!§0]§r Unkown player|Displayed when a requested player doesn't exist|
|flyspeed.noValue|§0[§c!§0]§r Please enter a valid value to set the flight speed to (0-10)|Displayed when the provided fly speed amount is invalid|
|actionBar|false|If to send all flight messages via the actionbar rather than in chat|

## Support

### PlaceHolderApi

If placeholder is detected the following placeholders will be available:

|Placeholder|Description|
|-|-|
|%fly_on%|If the player has flight enabled|
|%fly_off%|If the player has flight disabled|
|%fly_speed%|The players current flight speed|

Furthermore, any configuration messages will be able to interact with placeholders