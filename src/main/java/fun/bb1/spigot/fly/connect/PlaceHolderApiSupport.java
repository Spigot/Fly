package fun.bb1.spigot.fly.connect;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
public final class PlaceHolderApiSupport extends PlaceholderExpansion {
	
	private final @NotNull JavaPlugin plugin;
	
	@Internal
	public PlaceHolderApiSupport(final @NotNull JavaPlugin plugin) {
		this.plugin = plugin;
		if (this.register()) {
			plugin.getLogger().info("Placeholder support added");
		} else {
			plugin.getLogger().warning("Failed to add support for placeholder");
		}
	}
	
	@Override
	public final @NotNull String getIdentifier() {
		return "fly";
	}

	@Override
	public final @NotNull String getAuthor() {
		return "BradBot_1#2042";
	}

	@Override
	public final @NotNull String getVersion() {
		return "1.0.0";
	}
	
	@Override
	public final @Nullable String onPlaceholderRequest(final Player player, final @NotNull String params) {
		return super.onRequest(player, params);
	}
	
	@Override
	public final @Nullable String onRequest(final OfflinePlayer player, final @NotNull String params) {
		return switch(params.toLowerCase()) {
			case "active", "on" -> Boolean.toString(player.isOnline() && ((Player)player).getAllowFlight());
			case "inactive", "off" -> Boolean.toString(!player.isOnline() || !((Player)player).getAllowFlight());
			case "speed" -> Float.toString(player.isOnline() ? 0 : ((Player)player).getFlySpeed() * 10);
			default -> null;
		};
	}

}
