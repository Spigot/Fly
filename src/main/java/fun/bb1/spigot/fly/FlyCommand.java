package fun.bb1.spigot.fly;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.ApiStatus.Internal;

import fun.bb1.spigot.fly.config.Config;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
final class FlyCommand implements TabExecutor {
	
	private @NotNull final JavaPlugin plugin;
	private @NotNull final Config config;
	
	@Internal
	FlyCommand(@NotNull final JavaPlugin plugin, @NotNull final Config config) {
		this.plugin = plugin;
		this.config = config;
		final PluginCommand command = this.plugin.getCommand("fly");
		command.setExecutor(this);
		command.setTabCompleter(this);
	}
	
	@Override
	public final List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] arguments) {
		if (sender.hasPermission("fly.toggle.other")) return Bukkit.getOnlinePlayers().stream().map(Player::getName).toList();
		return List.of();
	}

	@Override
	public final boolean onCommand(CommandSender sender, Command command, String alias, String[] arguments) {
		if (arguments.length != 0) {
			if (!sender.hasPermission("fly.toggle.other")) {
				this.config.sendToggleOtherFailPermissionMessage(sender);
				return true;
			}
			for (final String playerName : arguments) {
				@Nullable final Player player = Bukkit.getPlayer(playerName);
				if (player == null) {
					this.config.sendToggleOtherFailPlayerMessage(sender);
					continue;
				}
				player.setAllowFlight(!player.getAllowFlight());
				player.setFlying(player.getAllowFlight());
				this.config.sendToggleMessage(player);
				this.config.sendToggleOtherMessage(sender, player);
			}
			return true;
		}
		if (!(sender instanceof Player player)) {
			sender.sendMessage("Cannot toggle flight for non-player");
			return true;
		}
		player.setAllowFlight(!player.getAllowFlight());
		player.setFlying(player.getAllowFlight());
		this.config.sendToggleMessage(player);
		return true;
	}

}
