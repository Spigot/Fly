package fun.bb1.spigot.fly;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import fun.bb1.spigot.fly.config.Config;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
final class FlySpeedCommand implements TabExecutor {
	
	private @NotNull final JavaPlugin plugin;
	private @NotNull final Config config;
	
	@Internal
	FlySpeedCommand(@NotNull final JavaPlugin plugin, @NotNull final Config config) {
		this.plugin = plugin;
		this.config = config;
		final PluginCommand command = this.plugin.getCommand("flyspeed");
		command.setExecutor(this);
		command.setTabCompleter(this);
	}
	
	@Override
	public final List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] arguments) {
		if (arguments.length <= 1) return List.of("1", "3", "5", "10");
		if (sender.hasPermission("flyspeed.toggle.other")) return Bukkit.getOnlinePlayers().stream().map(Player::getName).toList();
		return List.of();
	}

	@Override
	public final boolean onCommand(CommandSender sender, Command command, String alias, String[] arguments) {
		if (arguments.length == 0) {
			this.config.sendSetSpeedFailValueMessage(sender);
			return true;
		}
		final int value;
		try {
			value = Integer.parseInt(arguments[0]);
		} catch (NumberFormatException e) {
			this.config.sendSetSpeedFailValueMessage(sender);
			return true;
		}
		if (value < 0 || value > 10) {
			this.config.sendSetSpeedFailValueMessage(sender);
			return true;
		}
		if (arguments.length > 1) {
			if (!sender.hasPermission("flyspeed.toggle.other")) {
				this.config.sendSetSpeedOtherFailPermissionMessage(sender);
				return true;
			}
			for (int i = 1; i < arguments.length; i++) {
				@Nullable final Player player = Bukkit.getPlayer(arguments[i]);
				if (player == null) {
					this.config.sendSetSpeedOtherFailPlayerMessage(sender);
					continue;
				}
				player.setFlySpeed(value / 10f);
				this.config.sendSetSpeedMessage(player);
				this.config.sendSetSpeedOtherMessage(sender, player);
			}
			return true;
		}
		if (!(sender instanceof Player player)) {
			sender.sendMessage("Cannot toggle flight speed for non-player");
			return true;
		}
		player.setFlySpeed(value / 10f);
		this.config.sendSetSpeedMessage(player);
		return true;
	}

}
