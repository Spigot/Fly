package fun.bb1.spigot.fly.config;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
public final class Config {
	
	private final @NotNull Plugin plugin;
	
	@Internal
	public Config(final @NotNull Plugin plugin) {
		this.plugin = plugin;
	}
	
	private final void sendMessage(@NotNull final CommandSender sender, @NotNull String message) {
		if (sender instanceof Player player && this.plugin.getConfig().getBoolean("actionBar", false)) {
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(message));
			return;
		}
		sender.sendMessage(message);
	}
	
	private final @NotNull String placeholder(@NotNull final Player player, @NotNull final String message) {
		if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			return PlaceholderAPI.setPlaceholders(player, message);
		}
		return message;
	}
	
	public final void sendToggleMessage(@NotNull final Player sender) {
		this.sendMessage(sender, this.placeholder(sender, this.plugin.getConfig().getString(sender.getAllowFlight() ? "fly.self.enabled" : "fly.self.disabled", "§0[§a✓§0]§r You have toggled flight")));
	}
	
	public final void sendToggleOtherMessage(@NotNull final CommandSender sender, @NotNull final Player recipient) {
		this.sendMessage(sender, this.placeholder(recipient, this.plugin.getConfig().getString(recipient.getAllowFlight() ? "fly.other.enabled" : "fly.other.disabled", "§0[§a✓§0]§r You have toggled their flight")));
	}
	
	public final void sendToggleOtherFailPermissionMessage(@NotNull final CommandSender sender) {
		this.sendMessage(sender, this.plugin.getConfig().getString("fly.other.nopermission", "§0[§c!§0]§r You don't have permission to do that"));
	}
	
	public final void sendToggleOtherFailPlayerMessage(@NotNull final CommandSender sender) {
		this.sendMessage(sender, this.plugin.getConfig().getString("fly.other.notfound", "§0[§c!§0]§r Invalid player name"));
	}
	
	public final void sendSetSpeedMessage(@NotNull final Player sender) {
		this.sendMessage(sender, this.placeholder(sender, this.plugin.getConfig().getString("flyspeed.self.set", "§0[§a✓§0]§r Your flight speed hsa been changed")));
	}
	
	public final void sendSetSpeedOtherMessage(@NotNull final CommandSender sender, @NotNull final Player recipient) {
		this.sendMessage(sender, this.placeholder(recipient, this.plugin.getConfig().getString("flyspeed.other.set", "§0[§a✓§0]§r Their flight speed has been set")));
	}
	
	public final void sendSetSpeedOtherFailPermissionMessage(@NotNull final CommandSender sender) {
		this.sendMessage(sender, this.plugin.getConfig().getString("flyspeed.other.nopermission", "§0[§c!§0]§r You don't have permission to do that"));
	}
	
	public final void sendSetSpeedOtherFailPlayerMessage(@NotNull final CommandSender sender) {
		this.sendMessage(sender, this.plugin.getConfig().getString("flyspeed.other.notfound", "§0[§c!§0]§r Invalid player name"));
	}
	
	public final void sendSetSpeedFailValueMessage(@NotNull final CommandSender sender) {
		this.sendMessage(sender, this.plugin.getConfig().getString("flyspeed.noValue", "§0[§c!§0]§r Invalid player name"));
	}
	
}
