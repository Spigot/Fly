package fun.bb1.spigot.fly;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import fun.bb1.spigot.fly.config.Config;
import fun.bb1.spigot.fly.connect.PlaceHolderApiSupport;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
public class EntryPoint extends JavaPlugin {
	
	@Override
	public void onLoad() {
		this.saveDefaultConfig();
		super.onLoad();
	}
	
	@Override
	public void onEnable() {
		this.getLogger().info("Thank you for using flight!");
		if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			new PlaceHolderApiSupport(this);
		}
		final Config config = new Config(this);
		new FlyCommand(this, config);
		new FlySpeedCommand(this, config);
	}
	
}
